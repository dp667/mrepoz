﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TeamRoStats.Models;

namespace TeamRoStats.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Teams()
        {
            ViewBag.Message = "Random text";

            return View();
        }
        [Authorize(Roles = "admin")]
        public ActionResult Administrator()
        {
            ApplicationDbContext context = new ApplicationDbContext();

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            
            var roles = roleManager.Roles.ToList();
            var roleNames = new List<string>();
            foreach (var role in roles)
            {
                roleNames.Add(role.Name);
            }
           
        

            return View(roleNames);
        }
    }
}