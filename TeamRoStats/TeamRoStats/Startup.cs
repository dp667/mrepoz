﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TeamRoStats.Startup))]
namespace TeamRoStats
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
