﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TeamRoStats.Data;
using TeamRoStats.Models;

namespace TeamRoStats.Business
{
    public class MembershipBO : IMembershipBO
    {
        private IMembershipDA _userDA;
        public MembershipBO(IMembershipDA userDA)
        {
            _userDA = userDA;
        }

        public IEnumerable<Membership> GetUsers()
        {
            var result = _userDA.GetUsers();
            
            return result;
        }


    }
}