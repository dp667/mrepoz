﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TeamRoStats.Data;
using TeamRoStats.Models;

namespace TeamRoStats.Business
{
    public class ResultsBO : IResultsBO

    {
        private IResultsDA _resultsDA;
        public ResultsBO(IResultsDA resultsDA)
        {
            _resultsDA = resultsDA;

        }


        public TeamAverageModel GetAverageFor1stLine(DateTime endDate, DateTime startDate)
        {
            var result = _resultsDA.GetAverageFor1stLine(endDate, startDate);
            return result;
        }

        public TeamAverageModel GetAverageFor2ndLine()
        {
            throw new NotImplementedException();
        }

        public TeamAverageModel GetAverageForCalls()
        {
            throw new NotImplementedException();
        }

        public TeamAverageModel GetAverageForHosting()
        {
            throw new NotImplementedException();
        }
    }
}