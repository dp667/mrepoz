﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TeamRoStats.Business;
using TeamRoStats.Models;
using TeamRoStats.Models.Wrappers;

namespace TeamRoStats.API
{
    public class HomeController : ApiController
    {
        private IMembershipBO _membershipBO;
        private IResultsBO _resultsBO;
        public HomeController(IMembershipBO membershipBO, IResultsBO resultsBO)
        {
            _membershipBO = membershipBO;
            _resultsBO = resultsBO;

        }

        //[HttpGet]
        //public IEnumerable<Membership> GetUsers()
        //{
        //    var users = _membershipBO.GetUsers();
        //    return users;
        //}

        [HttpPost]
        public TeamAverageModel GetAverageFor1stLine(StartDateEndDateWrapper startDateEndDateWrapper)
        {
            if(startDateEndDateWrapper.EndDate == null)
            {
                startDateEndDateWrapper.EndDate = DateTime.Now;
            }
            if(startDateEndDateWrapper.StartDate == null)
            {
                startDateEndDateWrapper.StartDate = startDateEndDateWrapper.EndDate.Value.AddDays(-7);
            }
            var result = _resultsBO.GetAverageFor1stLine(startDateEndDateWrapper.EndDate.Value, startDateEndDateWrapper.StartDate.Value);
            

            return result;
        }
    }
}
