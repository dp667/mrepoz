﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TeamRoStats.Models;
using Dapper;

namespace TeamRoStats.Data
{
    public class MembershipDA : DataConn, IMembershipDA
    {
        

        public IEnumerable<Membership> GetUsers()
        {   

            using(var conn = ConnectionFactory())
            {
                conn.Open();
                var groupID = "'E0DD8C4B-F10D-457D-B409-FD6694AE7379'";
                var query = string.Format("SELECT * FROM userGroupMap WHERE GroupID = {0}", groupID);
                var result = conn.Query<Membership>(query);

                return result;
            }
        }

       
    }
}