﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TeamRoStats.Models;

namespace TeamRoStats.Data
{
    public class ResultsDA : DataConn, IResultsDA
    {
        public TeamAverageModel GetAverageFor1stLine(DateTime endDate, DateTime startDate)
        {
            using (var conn = ConnectionFactory())
            {
                conn.Open();
               
                var answerID = "'8197c252-eebc-44bd-b9bd-b9c1cb91ad2f'";
                var groupID = "'E0DD8C4B-F10D-457D-B409-FD6694AE7379'";
                var query = string.Format("select AVG(CAST(a.Response as numeric (5,2))) AS 'CSS' from SurveyResponse a " +
                    "join SurveyResult b on a.ResultID = b.ResultID " +
                    "join aspnet_Membership c on b.UserID = c.UserID " +
                    "join UserGroupMap d on c.UserID = d.UserID " +
                    "where b.UserID in (select UserID from aspnet_Membership where GroupID ={1}) " +
                    "and b.CreatedDate BETWEEN '{2}' AND '{3}' and a.AnswerID = {0} ", answerID, groupID, startDate, endDate);
                var result = conn.Query<decimal>(query);
                var teamAverageModel = new TeamAverageModel { Average = result.FirstOrDefault(), Name = "1st Line" };
                return teamAverageModel;
            }
        }

        public TeamAverageModel GetAverageFor2ndLine()
        {
            using (var conn = ConnectionFactory())
            {
                conn.Open();
                var answerID = "81C8497C-4645-4C53-BC22-49F363943CE9";
                var query = string.Format("select AVG(CAST(a.Response as numeric (5,2))) AS 'CSS' from SurveyResponse a " +
                    "join SurveyResult b on a.ResultID = b.ResultID " +
                    "join aspnet_Membership c on b.UserID = c.UserID" +
                    "join UserGroupMap d on c.UserID = d.UserID" +
                    "where b.UserID in (select UserID from aspnet_Membership where GroupID =@2ndline)" +
                    "and b.CreatedDate BETWEEN @StartDate AND @EndDate and a.AnswerID = {0}", answerID);
                var result = conn.Query<decimal>(query);
                var teamAverageModel = new TeamAverageModel { Average = result.FirstOrDefault(), Name = "2nd Line" };
                return teamAverageModel;
            }
        }

        public TeamAverageModel GetAverageForCalls()
        {
            throw new NotImplementedException();
        }

        public TeamAverageModel GetAverageForHosting()
        {
            throw new NotImplementedException();
        }
    }
}