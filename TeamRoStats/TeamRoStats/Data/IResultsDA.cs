﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeamRoStats.Models;

namespace TeamRoStats.Data
{
    public interface IResultsDA
    {
        TeamAverageModel GetAverageFor1stLine(DateTime endDate, DateTime startDate);
        TeamAverageModel GetAverageFor2ndLine();
        TeamAverageModel GetAverageForHosting();
        TeamAverageModel GetAverageForCalls();
                
    }
}
