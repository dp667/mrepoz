﻿using Autofac;
using Autofac.Integration.WebApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;
using TeamRoStats.API;
using TeamRoStats.Business;
using TeamRoStats.Controllers;
using TeamRoStats.Data;

namespace TeamRoStats.App_Start
{
    public class IoCConfig
    {
        public static void RegisterDependencies()
        {
            var builder = new ContainerBuilder();

            var config = GlobalConfiguration.Configuration;
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            // Business Objects

            builder.RegisterType<MembershipBO>().As<IMembershipBO>().InstancePerRequest();
            builder.RegisterType<ResultsBO>().As<IResultsBO>().InstancePerRequest();
            // Data Access Objects








            builder.RegisterType<ResultsDA>().As<IResultsDA>().InstancePerRequest();
            builder.RegisterType<MembershipDA>().As<IMembershipDA>().InstancePerRequest();
            //builder.RegisterType<TournamentTypeDA>().As<ITournamentTypeDA>().InstancePerRequest();

















            // Helpers


            // Register controllers
            builder.RegisterType<AccountController>().InstancePerRequest();
            builder.RegisterType<API.HomeController>().InstancePerRequest();

            //builder.RegisterType<DashboardController>().InstancePerRequest();

            builder.RegisterWebApiFilterProvider(config);
            builder.RegisterWebApiModelBinderProvider();




            var container = builder.Build();
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);



            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
        }
    }
}