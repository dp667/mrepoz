﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TeamRoStats.Models
{
    public class UserGroupMap
    {
        public System.Guid UserID { get; set; }
        public System.Guid GroupID { get; set; }
        public Nullable<System.DateTime> FromDate { get; set; }
        public Nullable<System.DateTime> ThruDate { get; set; }
        public byte[] Timestamp { get; set; }
    }
}