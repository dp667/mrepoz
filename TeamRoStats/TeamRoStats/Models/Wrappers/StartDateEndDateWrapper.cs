﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TeamRoStats.Models.Wrappers
{
    public class StartDateEndDateWrapper
    {
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}