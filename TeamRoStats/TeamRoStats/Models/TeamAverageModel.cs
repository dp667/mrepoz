﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TeamRoStats.Models
{
    public class TeamAverageModel
    {
        public string Name{ get; set; }
        public decimal Average { get; set; }
    }
}