﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TeamRoStats.Models
{
    public class SurveyResult
    {
        public System.Guid ResultID { get; set; }
        public System.Guid SurveyID { get; set; }
        public Nullable<System.Guid> TicketID { get; set; }
        public string CustomerID { get; set; }
        public Nullable<System.Guid> UserID { get; set; }
        public string CustomerUserName { get; set; }
        public Nullable<long> ChatSessionID { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<System.DateTime> CompletedDate { get; set; }
        public bool IsActive { get; set; }
        public Nullable<bool> SentBySystem { get; set; }
        public byte[] Timestamp { get; set; }
        public long ResultRef { get; set; }
        public Nullable<System.Guid> FollowUpSurveyID { get; set; }
        public Nullable<bool> FollowUpIsActive { get; set; }
        public Nullable<bool> IsTest { get; set; }
        public Nullable<decimal> FirstScore { get; set; }
        public Nullable<decimal> FollowUpScore { get; set; }
        public Nullable<System.DateTime> FollowUpCompletedDate { get; set; }
        public Nullable<System.Guid> AgentSurveyID { get; set; }
        public Nullable<int> FirstScoreNPS { get; set; }
        public Nullable<int> FollowUpScoreNps { get; set; }
        public Nullable<System.Guid> FollowUpUserID { get; set; }
        public Nullable<System.DateTime> AgentCompletedDate { get; set; }
        public Nullable<int> LifecycleIssue { get; set; }
        public string TriggerType { get; set; }
        public Nullable<int> MarketingScoreNPS { get; set; }
        public string PositiveSurveyFeedbackUrl { get; set; }
        public Nullable<bool> PositiveSurveyLinkClick { get; set; }
        public string Marketing123RefID { get; set; }
        public Nullable<System.DateTime> MarketingCreateDate { get; set; }
    }
}