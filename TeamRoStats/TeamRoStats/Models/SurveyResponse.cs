﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TeamRoStats.Models
{
    public class SurveyResponse
    {
        public System.Guid ResponseID { get; set; }
        public System.Guid ResultID { get; set; }
        public System.Guid AnswerID { get; set; }
        public string Response { get; set; }
        public byte[] Timestamp { get; set; }
    }
}